import Head from "next/head";
import { useEffect } from "react";
import { AlertWhenRouterIsUpdated } from "../components/AlertWhenRouterIsUpdated";
import { LinkList } from "../components/LinkList";

function Home() {
  useEffect(() => {
    const checkBfcache = (e) => {
      console.log("This page is restored from bfcache?", e.persisted);
      if (e.persisted) {
        alert("This page is served from bfcache");
      }
    };
    window.addEventListener("pageshow", checkBfcache);

    return () => {
      window.removeEventListener('pageshow', checkBfcache);
    }
  }, []);
  return (
    <>
      <Head>
        <title>Create Next App</title>
      </Head>
      <div>
        <AlertWhenRouterIsUpdated />
        <p className="margin">lorem1</p><p className="margin">lorem2</p><p className="margin">lorem3</p><p
        className="margin">lorem4</p><p className="margin">lorem5</p><p className="margin">lorem6</p><p
        className="margin">lorem7</p><p className="margin">lorem8</p><p className="margin">lorem9</p><p
        className="margin">lorem10</p><p className="margin">lorem11</p><p className="margin">lorem12</p><p
        className="margin">lorem13</p><p className="margin">lorem14</p><p className="margin">lorem15</p><p
        className="margin">lorem16</p><p className="margin">lorem17</p><p className="margin">lorem18</p><p
        className="margin">lorem19</p><p className="margin">lorem20</p><p className="margin">lorem21</p><p
        className="margin">lorem22</p><p className="margin">lorem23</p><p className="margin">lorem24</p><p
        className="margin">lorem25</p><p className="margin">lorem26</p><p className="margin">lorem27</p><p
        className="margin">lorem28</p><p className="margin">lorem29</p><p className="margin">lorem30</p><p
        className="margin">lorem31</p><p className="margin">lorem32</p><p className="margin">lorem33</p><p
        className="margin">lorem34</p><p className="margin">lorem35</p><p className="margin">lorem36</p><p
        className="margin">lorem37</p><p className="margin">lorem38</p><p className="margin">lorem39</p><p
        className="margin">lorem40</p><p className="margin">lorem41</p><p className="margin">lorem42</p><p
        className="margin">lorem43</p><p className="margin">lorem44</p><p className="margin">lorem45</p><p
        className="margin">lorem46</p><p className="margin">lorem47</p><p className="margin">lorem48</p><p
        className="margin">lorem49</p><p className="margin">lorem50</p><p className="margin">lorem51</p><p
        className="margin">lorem52</p><p className="margin">lorem53</p><p className="margin">lorem54</p><p
        className="margin">lorem55</p><p className="margin">lorem56</p><p className="margin">lorem57</p><p
        className="margin">lorem58</p><p className="margin">lorem59</p><p className="margin">lorem60</p><p
        className="margin">lorem61</p><p className="margin">lorem62</p><p className="margin">lorem63</p><p
        className="margin">lorem64</p><p className="margin">lorem65</p><p className="margin">lorem66</p><p
        className="margin">lorem67</p><p className="margin">lorem68</p><p className="margin">lorem69</p><p
        className="margin">lorem70</p><p className="margin">lorem71</p><p className="margin">lorem72</p><p
        className="margin">lorem73</p><p className="margin">lorem74</p><p className="margin">lorem75</p><p
        className="margin">lorem76</p><p className="margin">lorem77</p><p className="margin">lorem78</p><p
        className="margin">lorem79</p><p className="margin">lorem80</p><p className="margin">lorem81</p><p
        className="margin">lorem82</p><p className="margin">lorem83</p><p className="margin">lorem84</p><p
        className="margin">lorem85</p><p className="margin">lorem86</p><p className="margin">lorem87</p><p
        className="margin">lorem88</p><p className="margin">lorem89</p><p className="margin">lorem90</p><p
        className="margin">lorem91</p><p className="margin">lorem92</p><p className="margin">lorem93</p><p
        className="margin">lorem94</p><p className="margin">lorem95</p><p className="margin">lorem96</p><p
        className="margin">lorem97</p><p className="margin">lorem98</p><p className="margin">lorem99</p><p
        className="margin">lorem100</p><p className="margin">lorem101</p><p className="margin">lorem102</p><p
        className="margin">lorem103</p><p className="margin">lorem104</p><p className="margin">lorem105</p><p
        className="margin">lorem106</p><p className="margin">lorem107</p><p className="margin">lorem108</p><p
        className="margin">lorem109</p><p className="margin">lorem110</p><p className="margin">lorem111</p><p
        className="margin">lorem112</p><p className="margin">lorem113</p><p className="margin">lorem114</p><p
        className="margin">lorem115</p><p className="margin">lorem116</p><p className="margin">lorem117</p><p
        className="margin">lorem118</p><p className="margin">lorem119</p><p className="margin">lorem120</p><p
        className="margin">lorem121</p><p className="margin">lorem122</p><p className="margin">lorem123</p><p
        className="margin">lorem124</p><p className="margin">lorem125</p><p className="margin">lorem126</p><p
        className="margin">lorem127</p><p className="margin">lorem128</p><p className="margin">lorem129</p><p
        className="margin">lorem130</p><p className="margin">lorem131</p><p className="margin">lorem132</p><p
        className="margin">lorem133</p><p className="margin">lorem134</p><p className="margin">lorem135</p><p
        className="margin">lorem136</p><p className="margin">lorem137</p><p className="margin">lorem138</p><p
        className="margin">lorem139</p><p className="margin">lorem140</p><p className="margin">lorem141</p><p
        className="margin">lorem142</p><p className="margin">lorem143</p><p className="margin">lorem144</p><p
        className="margin">lorem145</p><p className="margin">lorem146</p><p className="margin">lorem147</p><p
        className="margin">lorem148</p><p className="margin">lorem149</p><p className="margin">lorem150</p><p
        className="margin">lorem151</p><p className="margin">lorem152</p><p className="margin">lorem153</p><p
        className="margin">lorem154</p><p className="margin">lorem155</p><p className="margin">lorem156</p><p
        className="margin">lorem157</p><p className="margin">lorem158</p><p className="margin">lorem159</p><p
        className="margin">lorem160</p><p className="margin">lorem161</p><p className="margin">lorem162</p><p
        className="margin">lorem163</p><p className="margin">lorem164</p><p className="margin">lorem165</p><p
        className="margin">lorem166</p><p className="margin">lorem167</p><p className="margin">lorem168</p><p
        className="margin">lorem169</p><p className="margin">lorem170</p><p className="margin">lorem171</p><p
        className="margin">lorem172</p><p className="margin">lorem173</p><p className="margin">lorem174</p><p
        className="margin">lorem175</p><p className="margin">lorem176</p><p className="margin">lorem177</p><p
        className="margin">lorem178</p><p className="margin">lorem179</p><p className="margin">lorem180</p><p
        className="margin">lorem181</p><p className="margin">lorem182</p><p className="margin">lorem183</p><p
        className="margin">lorem184</p><p className="margin">lorem185</p><p className="margin">lorem186</p><p
        className="margin">lorem187</p><p className="margin">lorem188</p><p className="margin">lorem189</p><p
        className="margin">lorem190</p><p className="margin">lorem191</p><p className="margin">lorem192</p><p
        className="margin">lorem193</p><p className="margin">lorem194</p><p className="margin">lorem195</p><p
        className="margin">lorem196</p><p className="margin">lorem197</p><p className="margin">lorem198</p><p
        className="margin">lorem199</p><p className="margin">lorem200</p><p className="margin">lorem201</p><p
        className="margin">lorem202</p><p className="margin">lorem203</p><p className="margin">lorem204</p><p
        className="margin">lorem205</p><p className="margin">lorem206</p><p className="margin">lorem207</p><p
        className="margin">lorem208</p><p className="margin">lorem209</p><p className="margin">lorem210</p><p
        className="margin">lorem211</p><p className="margin">lorem212</p><p className="margin">lorem213</p><p
        className="margin">lorem214</p><p className="margin">lorem215</p><p className="margin">lorem216</p><p
        className="margin">lorem217</p><p className="margin">lorem218</p><p className="margin">lorem219</p><p
        className="margin">lorem220</p><p className="margin">lorem221</p><p className="margin">lorem222</p><p
        className="margin">lorem223</p><p className="margin">lorem224</p><p className="margin">lorem225</p><p
        className="margin">lorem226</p><p className="margin">lorem227</p><p className="margin">lorem228</p><p
        className="margin">lorem229</p><p className="margin">lorem230</p><p className="margin">lorem231</p><p
        className="margin">lorem232</p><p className="margin">lorem233</p><p className="margin">lorem234</p><p
        className="margin">lorem235</p><p className="margin">lorem236</p><p className="margin">lorem237</p><p
        className="margin">lorem238</p><p className="margin">lorem239</p><p className="margin">lorem240</p><p
        className="margin">lorem241</p><p className="margin">lorem242</p><p className="margin">lorem243</p><p
        className="margin">lorem244</p><p className="margin">lorem245</p><p className="margin">lorem246</p><p
        className="margin">lorem247</p><p className="margin">lorem248</p><p className="margin">lorem249</p><p
        className="margin">lorem250</p><p className="margin">lorem251</p><p className="margin">lorem252</p><p
        className="margin">lorem253</p><p className="margin">lorem254</p><p className="margin">lorem255</p><p
        className="margin">lorem256</p><p className="margin">lorem257</p><p className="margin">lorem258</p><p
        className="margin">lorem259</p><p className="margin">lorem260</p><p className="margin">lorem261</p><p
        className="margin">lorem262</p><p className="margin">lorem263</p><p className="margin">lorem264</p><p
        className="margin">lorem265</p><p className="margin">lorem266</p><p className="margin">lorem267</p><p
        className="margin">lorem268</p><p className="margin">lorem269</p><p className="margin">lorem270</p><p
        className="margin">lorem271</p><p className="margin">lorem272</p><p className="margin">lorem273</p><p
        className="margin">lorem274</p><p className="margin">lorem275</p><p className="margin">lorem276</p><p
        className="margin">lorem277</p><p className="margin">lorem278</p><p className="margin">lorem279</p><p
        className="margin">lorem280</p><p className="margin">lorem281</p><p className="margin">lorem282</p><p
        className="margin">lorem283</p><p className="margin">lorem284</p><p className="margin">lorem285</p><p
        className="margin">lorem286</p><p className="margin">lorem287</p><p className="margin">lorem288</p><p
        className="margin">lorem289</p><p className="margin">lorem290</p><p className="margin">lorem291</p><p
        className="margin">lorem292</p><p className="margin">lorem293</p><p className="margin">lorem294</p><p
        className="margin">lorem295</p><p className="margin">lorem296</p><p className="margin">lorem297</p><p
        className="margin">lorem298</p><p className="margin">lorem299</p><p className="margin">lorem300</p><p
        className="margin">lorem301</p><p className="margin">lorem302</p><p className="margin">lorem303</p><p
        className="margin">lorem304</p><p className="margin">lorem305</p><p className="margin">lorem306</p><p
        className="margin">lorem307</p><p className="margin">lorem308</p><p className="margin">lorem309</p><p
        className="margin">lorem310</p><p className="margin">lorem311</p><p className="margin">lorem312</p><p
        className="margin">lorem313</p><p className="margin">lorem314</p><p className="margin">lorem315</p><p
        className="margin">lorem316</p><p className="margin">lorem317</p><p className="margin">lorem318</p><p
        className="margin">lorem319</p><p className="margin">lorem320</p><p className="margin">lorem321</p><p
        className="margin">lorem322</p><p className="margin">lorem323</p><p className="margin">lorem324</p><p
        className="margin">lorem325</p><p className="margin">lorem326</p><p className="margin">lorem327</p><p
        className="margin">lorem328</p><p className="margin">lorem329</p><p className="margin">lorem330</p><p
        className="margin">lorem331</p><p className="margin">lorem332</p><p className="margin">lorem333</p><p
        className="margin">lorem334</p><p className="margin">lorem335</p><p className="margin">lorem336</p><p
        className="margin">lorem337</p><p className="margin">lorem338</p><p className="margin">lorem339</p><p
        className="margin">lorem340</p><p className="margin">lorem341</p><p className="margin">lorem342</p><p
        className="margin">lorem343</p><p className="margin">lorem344</p><p className="margin">lorem345</p><p
        className="margin">lorem346</p><p className="margin">lorem347</p><p className="margin">lorem348</p><p
        className="margin">lorem349</p><p className="margin">lorem350</p><p className="margin">lorem351</p><p
        className="margin">lorem352</p><p className="margin">lorem353</p><p className="margin">lorem354</p><p
        className="margin">lorem355</p><p className="margin">lorem356</p><p className="margin">lorem357</p><p
        className="margin">lorem358</p><p className="margin">lorem359</p><p className="margin">lorem360</p><p
        className="margin">lorem361</p><p className="margin">lorem362</p><p className="margin">lorem363</p><p
        className="margin">lorem364</p><p className="margin">lorem365</p><p className="margin">lorem366</p><p
        className="margin">lorem367</p><p className="margin">lorem368</p><p className="margin">lorem369</p><p
        className="margin">lorem370</p><p className="margin">lorem371</p><p className="margin">lorem372</p><p
        className="margin">lorem373</p><p className="margin">lorem374</p><p className="margin">lorem375</p><p
        className="margin">lorem376</p><p className="margin">lorem377</p><p className="margin">lorem378</p><p
        className="margin">lorem379</p><p className="margin">lorem380</p><p className="margin">lorem381</p><p
        className="margin">lorem382</p><p className="margin">lorem383</p><p className="margin">lorem384</p><p
        className="margin">lorem385</p><p className="margin">lorem386</p><p className="margin">lorem387</p><p
        className="margin">lorem388</p><p className="margin">lorem389</p><p className="margin">lorem390</p><p
        className="margin">lorem391</p><p className="margin">lorem392</p><p className="margin">lorem393</p><p
        className="margin">lorem394</p><p className="margin">lorem395</p><p className="margin">lorem396</p><p
        className="margin">lorem397</p><p className="margin">lorem398</p><p className="margin">lorem399</p><p
        className="margin">lorem400</p>
        <LinkList />
      </div>
    </>
  );
}

// prevent '/' be static page (running as dynamic page)
Home.getInitialProps = async ({ req, res }) => {
  res?.setHeader("Cache-Control", "private, no-cache, must-revalidate");
  return {};
};

export default Home;
