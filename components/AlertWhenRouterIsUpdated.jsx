import { useRouter } from "next/router";
import React, { useEffect } from "react";

export const AlertWhenRouterIsUpdated = () => {
  const router = useRouter();

  return <h1>Currennt path: {router.asPath}</h1>;
};
